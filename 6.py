

def combine_values(num1, num2):

    if type(num1) == str and type(num2) == str:
        return num1 + num2

    elif type(num1) == int and type(num2) == int:
        return num1 * num2

    elif type(num1) == list and type(num2) == list:
        return num1 + num2

    elif type(num1) == dict and type(num2) == dict:
        return {**num1, **num2}

    else:
        return "not allowed"



print(combine_values("Anthony", " Nabalde"))
print(combine_values(15,56))
print(combine_values([1, 4, 3], [4, 5, 6]))
print(combine_values({'b': 6, 's': 2}, {'c': 3, 'd': 7}))
class Pet:
    def __init__(self, dog_name, dog_breed, dog_color):
        self.dog_name = dog_name
        self.dog_breed = dog_breed
        self.dog_color = dog_color

    def describe(self):
        return f"I bought a {self.dog_color} {self.dog_breed} and I named my dog {self.dog_name}"


dog = Pet("Max", "Labrador", "Black")


print(dog.describe())
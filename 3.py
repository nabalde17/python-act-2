def within_thousand(n):
   #absolute val
  return ((abs(1000 - n) <= 100) or (abs(2000 - n) <= 100))

print("1000 =",within_thousand(1000))
print("900 =",within_thousand(900))
print("800 =",within_thousand(800))
print("2200 =",within_thousand(2200))